<?php
$db_tiki='TIKI_DB_DRIVER';
$dbversion_tiki='TIKI_DB_VERSION';
$host_tiki='TIKI_DB_HOST';
$user_tiki='TIKI_DB_USER';
$pass_tiki='TIKI_DB_PASS';
$dbs_tiki='TIKI_DB_NAME';
$client_charset='utf8mb4';
// $dbfail_url = '';
// $noroute_url = './';
// If you experience text encoding issues after updating (e.g. apostrophes etc showing up as $
// $client_charset='latin1';
// $client_charset='utf8mb4';
// See http://tiki.org/ReleaseNotes5.0#Known_Issues and http://doc.tiki.org/Understanding+Enc$

// If your php installation does not not have pdo extension
// $api_tiki = 'adodb';

// Want configurations managed at the system level or restrict some preferences? http://doc.t$
// $system_configuration_file = '/etc/tiki.ini.php';
// $system_configuration_identifier = 'example.com';
