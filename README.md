## Tikiwiki 25 on Ubuntu 22.04 for Testing and Development by Carsten@tiki.org
This experiment is loosly based on https://github.com/fabiomontefuscolo/docker-tikiwiki
Removed haprox (as I intend to use jwilder reverse proxy) for replication we need a discssion.

This is not for production: it's for testing several recent tiki verions without the need to configure and install those manually.

PHP 8.3 is added for testing new php  version compatibility. Pls enable intended php version if needed by switching the a2enmod/a2dismod statements for 7.4/8.3 and set the php default accordingly in Dockerfile.

## Why?
I tried to build a working Docker Image with the ability to quickly <br> drop, update, reinstall tiki versions using php7.4. 

## Prerequisite
Docker installed
Basic usage know-how on Docker assumed. Only tested on Ubuntu.

## Because not needed here
I deleted tiki-install.php - so if you feel you need this pls remove/comment-out the 

RUN rm /var/www/html/tiki-install.php 

line in Dockerfile. 

## Maintenance and other tasks
You can use console.php through docker exec:

"docker exec containername php console.php command:option" 

as documented here: https://https://doc.tiki.org/Console

## Installation:
git clone into desired directory and change shell to it <br>

Edit .env

Run: bin/install.sh (if complayning about missing network create it via "docker network create yournetworkname" and rerun)<br>

Edit Dockerfile and uncomment desired target repository, if you wnt to build a different branch add it and comment out the current. 

The VAR  ${TIKI_INSTALL_VERSION}  is taken from .env. 

## IMPORTANT!
Create .env from _env_template to contain the variables. To enable copy environment template _env_template to .env file:<br>
cp _env_template .env <br>

Edit .env file via: <br>
nano .env 

or Editor of choice.

## Setup
If configerd with letsencrypt and jwilder (description will follow) visit your https://yourdomain
and immediately change admin password (default is what it is with tiki :-)) 

You also need to comment out the ports section in docker-compose.yml if using with reverse proxy
otherwise on local machine use http://localhost

When using locally uncomment ports section in docker-compose.yml

If defining other port in docker-compose.yml use http://localhost:yourport 
Note: in "ports: 80:80" first number is target last has to match EXPORT statement in Dockerfile.

use: "docker inspect −−format '{{ .NetworkSettings.IPAddress }}' containername" to get IP address of your container

## Troubleshooting
If docker complains about network please make sure to create the docker network beforehand via "docker network create yournetworkname" and set in .env file to existing docker network via TIKI_DOCKER_NETWORK variable.

If you get an error it maybe because of running on a slow machine.
Error: Command not available at this stage. Your database must be running and have valid credentials in the local.php file. See http://doc.tiki.org/Installation for more information.

On slow machines (eg using conventional old mechanical disks and not SSDs) its maybe neccessary to increase the sleep value in bin/install.sh

If apt throws an error from Dockerfile image creation: you need Docker >= 20.10.9 becuase of Ubuntu 22.04

If on docker-compose down theres an error "permision denied" it is maybe related to app armor. U can try to run <br>
"sudo aa-remove-unknown" to fix this.

## Update
From your install dir run ./bin/update.sh 

This leaves mysql tables untouched and pulls new files from repository.
If you want to wipe just run ./bin/install.sh again

Be patiant if accesing to early you get errors. If so, try a bit later
