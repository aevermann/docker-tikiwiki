FROM alpine/git AS downloader
ARG TIKI_INSTALL_VERSION=25.x
RUN mkdir -p /var/www \
#    && git clone --depth 1 --branch=feature/tiki-manager-gui https://gitlab.com/kroky/tiki.git /var/www/html
#    && git clone --depth 1 --branch=feature/tiki-manager https://gitlab.com/kroky/tiki.git /var/www/html
    && git clone --depth 1 --branch=${TIKI_INSTALL_VERSION} https://gitlab.com/tikiwiki/tiki.git /var/www/html
#    && git clone --depth 1 --branch=plugin-mautic-tiki https://gitlab.com/alvinBM/tiki.git /var/www/html
#    && git clone --depth 1 --branch=tiki-pop-up-builder https://gitlab.com/tikiwiki/tiki/-/tree/tiki-pop-up-builder /var/www/html
FROM ubuntu:22.04
LABEL mantainer "TikiWiki <tikiwiki-devel@lists.sourceforge.net>"
COPY --from=downloader /var/www/html /var/www/html
WORKDIR "/var/www/html"
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ARG DEBIAN_FRONTEND=noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get update && apt-get install nano git wajig composer apache2 software-properties-common curl -y
RUN add-apt-repository ppa:ondrej/php -y
RUN add-apt-repository ppa:ondrej/apache2 -y
RUN service apache2 restart
RUN apt-get update && apt-get install php7.4-cli php7.4 memcached \
php7.4-gettext poppler-utils bsdmainutils catdoc elinks php7.4-dom php-xml \
php7.4-tidy php7.4-gd php7.4-xmlrpc php7.4-mbstring php7.4-mysql php7.4-apcu php7.4-curl php7.4-intl \
php7.4-sqlite3 php7.4-zip  php7.4-pspell php7.4-memcached \
php7.4-soap libapache2-mod-php \
man-db odt2txt pstotext php7.4-common php7.4-bcmath php7.4-opcache php7.4-xml php7.4-zip \
php7.4-ldap php7.4-bz2 -y
RUN service apache2 stop && service apache2 start
RUN a2enmod rewrite 
RUN a2dismod php8.3
RUN a2enmod php7.4
RUN service apache2 stop && service apache2 start 
RUN update-alternatives --set php /usr/bin/php7.4
RUN service apache2 stop && service apache2 start 
RUN sh setup.sh composer
#RUN composer global require hirak/prestissimo \
#   && php \
#       -d zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so) \
#    && rm -rf /var/lib/apt/lists/* 

RUN rm -rf /tmp/* \ 
    && rm -rf /root/.composer \
    && {\
        echo "session.save_path=/var/www/sessions"; \
    }  > /etc/php/7.4/cli/conf.d/tiki_session.ini \
    && /bin/bash htaccess.sh \
    && mkdir -p /var/www/sessions \
    && mkdir -p /var/www/conf.d \
    && mkdir -p /var/www/html/modules/cache/ \
    && mkdir -p /var/www/html/whelp \
    && mkdir -p /var/www/html/mods \
    && mkdir -p /var/www/html/files \
    && chown -R www-data /var/www/sessions \
    && chown -R www-data /var/www/html/db/ \
    && chown -R www-data /var/www/html/dump/ \
    && chown -R www-data /var/www/html/img/trackers/ \
    && chown -R www-data /var/www/html/img/wiki/ \
    && chown -R www-data /var/www/html/img/wiki_up/ \
    && chown -R www-data /var/www/html/modules/cache/ \
    && chown -R www-data /var/www/html/temp/ \
    && chown -R www-data /var/www/html/files/ \
    && chown -R www-data /var/www/html/templates/
VOLUME ["/var/www/conf.d/","/var/www/html/files/","/var/www/html/img/trackers/","/var/www/html/img/wiki_up/","/var/www/html/img/wiki/","/var/www/html/modules/cache/","/var/www/html/storage/","/var/www/html/temp/","/var/www/sessions/"]
RUN sh setup.sh composer
COPY config/php.ini /etc/php/7.4/cli/conf.d
COPY config/local.php /var/www/html/db/local.php
COPY bin/start-tiki /usr/bin 
RUN chmod 755 /usr/bin/start-tiki
RUN export TIKI_PATH=$TIKI_PATH:/var/www/html
RUN service apache2 stop && service apache2 start
#RUN rm tiki-install.php
EXPOSE 80
CMD ["start-tiki"]
