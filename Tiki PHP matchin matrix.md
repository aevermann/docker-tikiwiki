| Tiki Release | Prerequisite PHP Versions | Compatible PHP Versions                   |
|--------------|---------------------------|-------------------------------------------|
| Tiki 18       | PHP 5.4 or later           | PHP 5.4, 5.5, 5.6, 7.0, 7.1, 7.2          |
| Tiki 19       | PHP 5.6 or later           | PHP 5.6, 7.0, 7.1, 7.2, 7.3               |
| Tiki 20       | PHP 7.0 or later           | PHP 7.0, 7.1, 7.2, 7.3, 7.4, 8.0          |
| Tiki 21       | PHP 7.1 or later           | PHP 7.1, 7.2, 7.3, 7.4, 8.0, 8.1          |
| Tiki 22       | PHP 7.2 or later           | PHP 7.2, 7.3, 7.4, 8.0, 8.1               |
| Tiki 23       | PHP 7.3 or later           | PHP 7.3, 7.4, 8.0, 8.1, 8.2               |
| Tiki 24       | PHP 7.4 or later           | PHP 7.4, 8.0, 8.1, 8.2                   |
| Tiki 25       | PHP 8.0 or later           | PHP 8.0, 8.1, 8.2                         |
| Tiki 26       | PHP 8.1 or later           | PHP 8.1, 8.2                             |
| Tiki 27       | PHP 8.1 or later           | PHP 8.1, 8.2                             |
