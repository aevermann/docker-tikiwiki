#!/usr/bin/env bash

thispathdir=$(pwd)
currentdir=$(basename "$thispathdir")

docker compose down --volumes &&  docker compose up --build --force-recreate -d
sleep 60
docker exec "$currentdir"-tiki-1 php console.php database:install
docker exec "$currentdir"-tiki-1 sh setup.sh fix y 
docker exec "$currentdir"-tiki-1 php console.php index:rebuild 
docker exec "$currentdir"-tiki-1 php console.php installer:lock
