#!/usr/bin/env bash

thispathdir=$(pwd)
currentdir=$(basename "$thispathdir")

docker compose down &&  docker image rm "$currentdir"-tiki && docker compose up -d
sleep 60
docker exec "$currentdir"-tiki-1 php console.php database:update
sleep 20
docker exec "$currentdir"-tiki-1 sh setup.sh fix y
sleep 20
docker exec "$currentdir"-tiki-1 php console.php index:rebuild
sleep 20
docker exec "$currentdir"-tiki-1 php console.php installer:lock
